lpr200 package
==============

.. automodule:: lpr200

MiniSeed
--------
Principal class for reading and manipulating a collection of LPR-200
miniseed files.

.. autoclass:: MiniSeed



Helper Functions
----------------

.. autofunction:: read_first_header
.. autofunction:: get_miniseed_starttime
.. autofunction:: is_lpr200_mseed
.. autofunction:: btime_to_utc
.. autofunction:: header_btime_to_utc
.. autofunction:: datetime64_to_utc

Helper Classes
--------------
Classes for facilitating 'repairs' on LPR-200 miniseed output

.. autoclass:: GapFixer
    :members: index, top, file_namer, verbose, fix_channel, fix_station, fix_all

.. autoclass:: BadFileFixer
    :members: index, top, channel_codes, dry_run, print_summary,
        fix_missing_files, fix_weird_file_names

Exceptions
----------
.. autoexception:: LPR200FormatError
.. autoexception:: LPR200TimeGapError

Submodules
----------

.. toctree::

   lpr200.log

