.. lpr200 documentation master file, created by
   sphinx-quickstart on Mon Mar 12 12:11:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lpr200's documentation!
==================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   lpr200


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
