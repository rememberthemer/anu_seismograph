"""Work with time correction indices."""

import numpy as np
import obspy

__all__ = ['Index']

from lpr200.miniseed import datetime64_to_utc


class Index:
    """Clock correction indices for merged LPR200 MiniSeed files.

    Get the fraction clock correction for """
    def __init__(self, *indices):

        self.files = []
        self.index = {}
        if indices:
            for i in indices:
                self.load(i)

    def load(self, fname):
        """Load pick indices.

        Parameters
        ----------
        fname : str
            Index files to load

        """
        self.files.append(fname)
        fname.split('.')
        # read the file
        with open(fname) as idx_file:
            stn = idx_file.readline().strip()
            chan = idx_file.readline().strip()
            tt = [l.strip().split() for l in idx_file]
        # create entries in index
        if stn not in self.index:
            self.index[stn] = {}
        if chan not in self.index[stn]:
            self.index[stn][chan] = {'utc': [], 'corr': []}

        utc = [obspy.UTCDateTime(t[0]) for t in tt]
        self.index[stn]['utc'] = np.array(utc, dtype='datetime64[us]')
        self.index[stn]['corr'] = np.int64([t[1] for t in tt])

    def get_shift(self, t, stationid=None):
        """Get fractional clock correction at time 't'

        Parameters
        ----------
        stationid : str
        t : datetime-like or str
            The time at which to determine correction. `t` will be converted
            to a numpy.datetime64, so  any `datetime` like object
            (e.g. `obspy.UTCDateTime`, `numpy.datetime64`) or datetime parsable
            string will work.

        """
        if (stationid not in self.index or
            channelid not in self.index[stationid]
        ):
            return 0.
        idx = self.index[stationid][channelid]
        st = np.datetime64(t, 'us')
        # check if t is in time range covered by index
        if st <= idx['utc'][0] or st >= idx['utc'][-1]:
            return 0.
        # get the first time in index that is > st and make a mask
        i = np.argwhere(st < idx['utc'])[0, 0]
        t0, t1 = idx['utc'][i-1:i+1]
        c0, c1 = idx['corr'][i-1:i+1]
        return 1e-6 * (c1 - c0) * (st - t0) / (t1 - t0)

    def get_trace_shift(self, trace):
        """Get fractional clock correction of `trace`.

        Parameters
        ----------
        trace : obspy.Trace
            The `trace` object to shift

        """
        return self.get_shift(trace.stats.station, trace.stats.starttime)
