"""Work with ANU LPR-200 log files.

Classes and function for dealing with log files written by an
ANU LPR-200 seismograph.

"""

import os
import io
import struct

import obspy


# seismometer_types = {0: "LennartzLE-3DlITE", 1: "GuralpCMG-40T",
#                      2: "GuralpCMG-3ESP", 3: "TrilliumCompact",
#                      4: "MarkL4", 5: "MarkL4C"}

__all__ = ['LogSession', 'LogGPSRecord', 'read_logs']


class LogSession(object):
    """Read and manipulate an ANU LR-200 log 'session'.

    Because a log file may contain several discrete log 'sessions', you
    probably don't want to initialise this class directly. Instead call the
    `read_logs` function to read a log file and return a list of contained
    LogSession objects.

    Parameters
    ----------
    fname : str
        The log file to be _read
    offset : int, optional
        Start reading file from `offset` byte
        default: 0

    Attributes
    ----------
    params : dict
        The non 'GPS' log records
    gps : list
        The 'GPS' log records
    file : dict
        Keys are 'file' = name of file log session read from
        and 'bytes' = [start, end] byte range from which log data were read.

    """

    _gps_words = {'UDF': b'>6i', 'GPS': b'>6i3d3i'}
    _param_words = {'BSN': b'>i', 'FWV': b'>22s', 'SPR': b'>i', 'SMM': b'>i',
                    'SMS': b'>10s', 'RCS': b'>6i', 'RCE': b'>6i'}

    def __init__(self, fname, offset=0):
        # noinspection PyArgumentList
        self.params = dict(zip(self._param_words.keys(),
                               [None] * len(self._param_words)))
        self.gps = []
        self.file = {'name': '', 'bytes': [-1, -1]}
        self.read(fname, offset=offset)

    def __len__(self):
        """Length of gps attribute array."""
        return len(self.gps)

    def _read_binary_record(self, word, fh):
        """Read a binary of record of type `word` from open file `fh`."""
        fmt = struct.Struct(self._param_words[word])
        dat = fmt.unpack(fh.read(fmt.size))
        if len(dat) == 1:
            return dat[0]
        else:
            return list(dat)

    def read(self, fname, offset=0):
        """Read a log 'session' from `fname` starting at `offset` byte.

        Parameters
        ----------
        fname : str or io.FileIO
            The input file or open file handle.
        offset : int
            Start reading from 'offset' bytes

        """
        # handle either IO objects or file name strings strings
        if isinstance(fname, io.FileIO):
            fh = fname
            fown = False
        else:
            fh = open(fname, 'rb')
            fown = True

        #     go to record start in LOG
        fh.seek(int(offset))
        self.file['bytes'][0] = fh.tell()
        self.file['name'] = fh.name

        fsize = os.stat(self.file['name']).st_size
        while fh.tell() < fsize:
            # read 3 bytes and convert to str
            word = fh.read(3).decode("ascii").upper()
            if word.startswith('GP'):
                self.gps.append(LogGPSRecord(fh))
            elif word == 'UDF':
                # skip UDF records for now
                fh.seek(36, 1)
            elif word in self._param_words:
                if word == 'RCS':
                    if self.params['RCS'] is not None:
                        # we have found the start of a new log record
                        # but have not found RCE that terminates the
                        # record we were reading
                        # => unterminated record e.g. power pulled etc
                        # => rewind 3 bytes and stop
                        fh.seek(-3, 1)
                        self.file['bytes'][1] = fh.tell()
                        break

                # _read other word types
                self.params[word] = self._read_binary_record(word, fh)

                if word == 'RCE':
                    # This is last entry for a log record, so stop
                    self.file['bytes'][1] = fh.tell()
                    break
            else:
                raise ValueError("Reading log file '{}': unrecognised log "
                                 "record type '{}' at byte {}"
                                 .format(fh.name, word, fh.tell() - 3))
        if fown:
            fh.close()

        # clean up gps by removing bung records
        self.gps = [g for g in self.gps if g]

    @property
    def starttime(self):
        """Time of RCS (i.e. start) record."""
        if self.params['RCS']:
            dt = self.params['RCS']
            return obspy.UTCDateTime(year=dt[0], julday=dt[1],
                                     hour=dt[2], minute=dt[3],
                                     second=dt[4], microsecond=dt[5])
        return None

    @property
    def endtime(self):
        """Time of RCE (i.e. final) record."""
        if self.params['RCE']:
            dt = self.params['RCE']
            return obspy.UTCDateTime(year=dt[0], julday=dt[1],
                                     hour=dt[2], minute=dt[3],
                                     second=dt[4], microsecond=dt[5])
        return None

    def filter_gps(self):
        """Strip GPS records with bad times."""
        self.gps = [g for g in self.gps if g]


class LogGPSRecord(object):
    """Read and interpret a GPS entry in a LPR-200 Log.

    LogGPSRecord objects are instantiated as part of generating/reading
    a LogSession object. You probably do not want to use this class directly.

    Parameters
    ----------
    fh : io.FileIO
        An open file/stream from which gps record will be read.

    """

    fmt = struct.Struct(b'>6i3d3i')

    def __init__(self, fh):
        self.byte = -1
        self.record = []
        self.time = None
        self.byte = fh.tell() - 3

        try:
            self.record = list(self.fmt.unpack(fh.read(self.fmt.size)))
            self.time = obspy.UTCDateTime(year=self.record[2],
                                          month=self.record[1],
                                          day=self.record[0],
                                          hour=self.record[3],
                                          minute=self.record[4],
                                          second=self.record[5])
        except (TypeError, ValueError):
            pass

    def __bool__(self):
        """Time has been _read/set."""
        return bool(self.record) and isinstance(self.time, obspy.UTCDateTime)

    @property
    def position(self):
        """Return geographical position."""
        return tuple(self.record[6:9]) if self.record else ()

    @property
    def clock_error(self):
        """Return the clock error."""
        return self.record[9] if self.record else 0


def read_logs(filename):
    """Read an LPR-200 log file and break into discrete log 'sessions'.

    A LOG record is defined by RCS (start) and RCE (end) fields. A single log
    file may hold several discrete sessions, each corresponding to a discrete
    continuous recording interval

    Parameters
    ----------
    filename : str
        log file to be read

    Returns
    -------
    list
        LogSession objects

    """
    records = []
    file_size = os.stat(filename).st_size
    start_byte = 0
    while start_byte < file_size:
        records.append(LogSession(filename, start_byte))
        start_byte = records[-1].file['bytes'][1]
    return records
