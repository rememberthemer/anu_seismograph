"""Miniseed classes."""

import os
import io
import struct

import numpy as np
import obspy

__all__ = [
    'LPR200FormatError', 'LPR200TimeGapError', 'MiniSeed',
    'LPR200_MSEED_HEADER_DTYPE', 'read_first_header', 'is_lpr200_mseed',
    'hdr_to_datetime64', 'datetime64_to_utc'
]

HDR_NAMES = ('SeqNum', 'QualityIndicator', 'Unused1', 'StationID',
             'LocationID', 'ChannelID', 'NetworkCode', 'y', 'j', 'H', 'M', 'S',
             'Unused2', 'us', 'nSamp', 'SampleRateFactor',
             'SampleRateMultiplier', 'ActivityFlags', 'IO_ClockFlags',
             'DataQualityFlags', 'nBlocks', 'T_corr', 'DataBegin',
             'FirstBlock', 'BlockId', 'NextBlock', 'Encoding', 'WordOrder',
             'DataRecLen', 'Unused3')
"""Miniseed header word names used."""

HDR_FORMATS = ('S6', 'S1', 'S1', 'S5', 'S2', 'S3', 'S2', '>u2', '>u2', '>u1',
               '>u1', '>u1', 'S1', '>u2', '>u2', '>i2', '>i2', '>u1', '>u1',
               '>u1', '>u1', '>i4', '>u2', '>u2', '>u2', '>u2', '>i1', '>i1',
               '>i1', 'S1')
"""Miniseed header word types, including byte-order."""

# noinspection PyArgumentList
LPR200_MSEED_HEADER_DTYPE = np.dtype(list(zip(HDR_NAMES, HDR_FORMATS)))
"""dtype : Structured data type for interpreting miniseed 'volumes'.

Format is specific to miniseed 'data only' volumes as written by an
ANU LPR-200 seismograph.
"""


class LPR200FormatError(ValueError):
    """Exception for LPR200 Miniseed format errors."""

    pass


class LPR200TimeGapError(ValueError):
    """Exception for unresolvable time gap errors."""

    pass


class MiniSeed:
    """Read, manipulate and fix miniseed records from a LPR-200 seismograph.

    Miniseed volumes written ANU LPR-200 seismographs may contain a variety of
    time related errors that render them unusable. These errors include:
        1.  Random gaps/jumps between successive volumes within a file
        2.  Unreadable time headers. E.g. the seconds field being 60,
            while valid range is 0-59
        3.  Incorrectly incremented year at crossover to a new year.
            E.g. 2017 -> 2024.
        4.  File names where the time component is garbage

    In addition to these
    haphazardly applied clock corrections. This class provides methods
    for reading a series of LPR-200 miniseed fles and correcting such issues.

    The various corrections are onle possible on a 'recording session' basis.
    That is by processing all files written between a recording
    start and stop as a single block or mega-file.  The critical assumption is
    that the first file in a session will start with no clock

    Parameters
    ----------
    *input_files:
        Filename(s) to be read.

    Attributes
    ----------
    volumes : numpy.ndarray (dtype=structured)
        1d array of miniseed volume headers stored as a structured numpy dtype.
        Use ``volumes.dtype`` to access dtype parameters.
    times : numpy.ndarray (dtype='datetime64[100us]')
        1d array of start times for every header in `volumes`. Derived directly
        from the 'y', 'j', 'H', 'M', 'S' and 'us' header word. Note that the
        numpy.datetime64 increment is 100 u-seconds, which is the maximum
        permissable resolution for a miniseed file.
    gaps : numpy.ndarray (dtype=structured) or None
        Array of 'gaps' between individual data blocks or None if no gaps
        exist. Dtype is ``[('i', uint64), 'dt': numpy.timedelta64[us])]`` where
        `i` = index in `volumes` where gap exists and `dt` = gap in
        microseconds.
    clock_corrs : numpy.ndarray (dtype=structured)
        Array of time corrections `updates` to reflecting drift of inetrnal
        clock from UTC time. Updated when a new correction is obtained from
        a GPS time sync (approximately hourly).
        Dtype is `[('i', int64), 'corr': 'timedelta64[100us]']` where
        `i` = index in `volumes` and `corr` = correction in 100 usecond
        increments.
    files : list
        List of dicts holding input file parameters, sorted by file starttime.
        Keys are :
            'name': filename,
            'starttime': the start time as an obspy.UTCDateTime instance
            'n': the number of discrete volumes in the file,
            'index': (istart, iend), the indices in `volumes` array
                where headers read from the file start and end.
    stats : StatsDict
        Summary/meta data for `volumes` array and processing flags.

    Raises
    ------
    LPR200FormatError
        Raised when input files have inconsistent values in headers that
        should be constant. E.g. Varying 'ChannelID' indicates attempting to
        merge, say 'N' and 'E' components. Header words checked are
        ('StationID', 'LocationID', 'ChannelID', 'NetworkCode', 'nSamp',
        'SampleRateFactor', 'SampleRateMultiplier')

    """

    dtypes = {
        'hdrs': LPR200_MSEED_HEADER_DTYPE,
        'times': np.dtype('datetime64[100us]'),
        'gap': np.dtype([('i', 'int64'), ('dt', 'timedelta64[100us]')]),
        'tcorr': np.dtype([('i', 'int64'), ('corr', 'timedelta64[100us]')])
    }

    def __init__(self, *input_files):
        self.volumes = None
        self.times = None
        self.gaps = None
        self.clock_corrs = None
        self.files = []
        self.stats = StatsDict()

        # Read the files
        if len(input_files) == 0:
            raise ValueError("no input files provided")

        # get important parameters from first file
        self._set_stats(input_files[0])

        raw = []

        # form files and ifiles arrays
        bs = self.stats.block_nbytes
        hs = 56
        for i, fname in enumerate(input_files):

            hdr = read_headers(fname, block_size=bs)
            raw.append(hdr)
            nhdr = len(hdr) // hs
            # noinspection PyTypeChecker
            utc = datetime64_to_utc(hdr_to_datetime64(hdr[:56]))

            # temp is a sort key, gets removed below
            self.files.append({
                'n': nhdr,
                'name': fname,
                'starttime': utc,
                'temp': i
            })

        # sort arrays by starttime
        self.files = sorted(self.files, key=lambda x: x['starttime'])

        # set index in files array
        i_end = np.cumsum([f['n'] for f in self.files]).tolist()
        i_start = [0] + i_end[:-1]

        for i in range(len(self.files)):
            # noinspection PyTypeChecker
            self.files[i]['index'] = (i_start[i], i_end[i])

        # form the volumes array by joining sorted blocks
        indices = [f.pop('temp') for f in self.files]
        raw_hdrs = b''.join((raw[i] for i in indices))

        # noinspection PyTypeChecker
        self.volumes = np.fromstring(raw_hdrs, dtype=self.dtypes['hdrs'])

        # these values must remain consistent for all headers
        check_words = ('StationID', 'LocationID', 'ChannelID', 'NetworkCode',
                       'nSamp', 'SampleRateFactor', 'SampleRateMultiplier')
        for word in check_words:
            if not np.all(self.volumes[word] == self.volumes[word][0]):
                raise LPR200FormatError(
                    "values for header word '{}' are inconsistent across "
                    "input files.".format(word))

        self.__set_time_arrays__()

    def __len__(self):
        """Length of volumes attribute."""
        return len(self.volumes)

    # noinspection PyUnresolvedReferences
    def _set_stats(self, fname):
        """Set numpy dtype used to read miniseed volumes written by a LPR200.

        Read first fixed header and data blockette header from `fname` and
        determine byte order, record sizes, sampling rates and data formats.

        Parameters
        ----------
        fname : str
            File from which headers will be read.

        """
        hdr1 = read_first_header(fname)

        self.stats.block_nbytes = 2**int(hdr1['DataRecLen'])
        self.stats.block_nsamp = int(hdr1['nSamp'])

        sr = float(hdr1['SampleRateFactor'])
        srm = float(hdr1['SampleRateMultiplier'])
        if sr < 0:
            sr = 1 / sr
        if srm > 0:
            sr *= srm
        elif srm < 0:
            sr /= srm
        self.stats.samplerate = float(sr)

        self.stats.delta = int(round(10000 * (1 / self.stats.samplerate)))
        self.stats.block_len = self.stats.block_nsamp * self.stats.delta
        self.stats.channelid = hdr1['ChannelID'].decode().strip()
        self.stats.stationid = hdr1['StationID'].decode().strip()
        self.stats.networkid = hdr1['NetworkCode'].decode().strip()

    def __set_time_arrays__(self):
        """Set the various time arrays.

        Called by methods that alter times (e.g. `fix_gap_errors`).

        """
        # create a times array
        # this method looks cumbersome but is 100's of times faster
        # (1) create datetime array from years only
        # (2) add remaining j, h, m, s, us as timedelta's of appropriate
        # resolution

        y = np.asarray(self.volumes['y'].astype(str), dtype='datetime64[Y]')
        self.times = (
            y.astype(self.dtypes['times']) +
            np.asarray(self.volumes['j'] - 1, dtype='<m8[D]') +
            np.asarray(self.volumes['H'], dtype='<m8[h]') +
            np.asarray(self.volumes['M'], dtype='<m8[m]') +
            np.asarray(self.volumes['S'], dtype='<m8[s]') +
            np.asarray(self.volumes['us'], dtype='<m8[100us]')
        )

        # Headers may contain badly formatted BTIME words
        # where seconds > 59. Creating times array corrects this,
        # so write times back into fixed headers
        if np.any(self.volumes['S'] > 59):
            pass
            self.__write_times_to_headers__()

        # find time shift errors between consecutive volumes
        no_gap = np.timedelta64(0, '100us')

        gap = self.times[1:] - self.times[:-1] - self.stats.block_len
        igap = np.squeeze(np.argwhere(gap != no_gap), axis=1)
        self.gaps = np.empty_like(igap, dtype=self.dtypes['gap'])
        if len(igap) > 0:
            self.gaps['i'] = igap.copy() + 1
            self.gaps['dt'] = gap[igap]

        # Determine where clock corrections have been applied
        cc = self.volumes['T_corr']

        icc = np.where((cc[1:] != cc[:-1]))
        icc = icc[0] + 1

        self.clock_corrs = np.zeros(icc.size + 1, dtype=self.dtypes['tcorr'])
        self.clock_corrs['i'][1:] = icc
        self.clock_corrs['corr'] = cc[self.clock_corrs['i']]

    def __write_times_to_headers__(self):
        """Write times back into the header blocks.

        Called by method that alter times (e.g. `fix_gap_errors`)

        """
        y = self.times.astype('datetime64[Y]')
        j = (self.times - y).astype('<m8[D]')
        h = (self.times - y - j).astype('<m8[h]')
        m = (self.times - y - j - h).astype('<m8[m]')
        s = (self.times - y - j - h - m).astype('<m8[s]')
        us = (self.times - y - j - h - m - s).astype('<m8[100us]')

        self.volumes['y'] = y.astype(int) + 1970
        self.volumes['j'] = j.astype(int) + 1
        self.volumes['H'] = h.astype(int)
        self.volumes['M'] = m.astype(int)
        self.volumes['S'] = s.astype(int)
        self.volumes['us'] = us.astype(int)

    def find_data_breaks(self, max_gap=600, unit='s', check_cc=True):
        """Find 'real' gaps in data due to missing files, recording stops etc.

        Parameters
        ----------
        max_gap : int or numpy.timedelta64, optional
            Time gaps > `max_gap` seconds are assumed to be real data_gaps
            (default is 600 seconds, i.e. 10 minutes).
        unit : str, optional
            The unit of `max_gap`. Can be any string understood by the
            `numpy.timedelta64()` method. (Default is 's', max_gap is in
            seconds). Ignored if `max_gap` is a timedelta64 instance.
        check_cc : bool, optional
            Cross check found 'breaks' against clock corrections. Real 'breaks'
            i.e. recording stops, should have a concomitant disruption in clock
            corrections as the internal clock/GPS is reset. If there is no
            disruption in clock corrections, then that 'break' is assumed to be
            a standard LPR200 data write error of the type we want to correct
            and will be ignored. (Default is True, do cross checking).

        """
        if self.has_gaps:
            if not isinstance(max_gap, np.timedelta64):
                max_gap = np.timedelta64(max_gap, unit)
            rval = np.where(self.gaps[:]['dt'] > max_gap)[0]

            # test if gaps have associated clock corr jumps
            if rval.size > 0 and check_cc and self.clock_corrs.size > 10:
                test_cc = 5 * np.int64(
                    np.mean(
                        np.abs(
                            self.clock_corrs['corr'][1:] -
                            self.clock_corrs['corr'][:-1]
                        )
                    )
                )

                mask = []
                file_starts = [f['index'][0] for f in self.files]
                for i in rval:
                    dcc = self.volumes[i]['T_corr'] - \
                          self.volumes[i-1]['T_corr']

                    # its a big step, > 5 * mean step, indicates
                    # real time break => real recording break
                    is_big = np.abs(dcc) >= test_cc

                    # Is close to 0, so GPS clock probably reset
                    near_zero = self.volumes[i]['T_corr'] < 10

                    # at the start of a data file, recording has restarted
                    at_file_start = i in file_starts

                    # all criteria must be met
                    mask.append(is_big & near_zero & at_file_start)

                # mask out breaks that are data write errors
                rval = rval[mask]

        else:
            # an empty array
            rval = np.ndarray((0, ), dtype=np.int64)

        return rval

    def fix_gap_errors(self, max_gap=600, unit='s', check_cc=True):
        """Correct time gap errors in headers.

        Data volumes are assumed to be contiguous, so any gap between
        successive volume start times is assumed to be a gap error. Use the
        `max_gap` parameter to avoid merging 'real' gaps.

        Parameters
        ----------
        max_gap : int or numpy.timedelta64, optional
            Raise `ValueError` if any gaps > `max_gap` seconds. 0 = no gap
            length checking. Default: 600, i.e. 10 minutes.
        unit : str, optional
            The unit of `max_gap`. Can be any string understood by the
            `numpy.timedelta64()` method. (Default is 's', max_gap is in
            seconds). Ignored if `max_gap` is a timedelta64 instance.
        max_gap: int, optional
            Maximum permissible time gap (in seconds) between successive
            headers. If `max_gap` is exceeded then this could indicate a REAL
            break in recording and a `LPR200TimeGapError` exception
            will be raised. (Default is 600 seconds, i.e 10 minutes)
        check_cc : bool, optional
            Cross check found 'breaks' against clock corrections. See
            `find_data_breaks`. (Default is True, do cross checking).

        Returns
        -------
        total_shift : numpy.timedelta64
            The total shift applied to the last volume. Microsecond basis
            numpy.timedelta64 or `'<m8[us]'`

        """
        if not self.has_gaps:
            return 0

        # bomb out if we find a big gap
        if max_gap:
            if not isinstance(max_gap, np.timedelta64):
                max_gap = np.timedelta64(max_gap, unit)

            big_gaps = self.find_data_breaks(max_gap=max_gap,
                                             check_cc=check_cc)

            if big_gaps.size > 0:
                mg = np.timedelta64(max_gap, 's').astype(int)
                raise LPR200TimeGapError(
                    "data contain gaps larger than {} seconds "
                    "=> real data breaks?".format(mg))

        incr = np.timedelta64(self.stats.block_len, '100us')
        endtime = self.starttime + self.times.size * incr
        newtimes = np.arange(self.starttime, endtime, incr)
        self.times[:] = newtimes

        self.__write_times_to_headers__()
        self.__set_time_arrays__()
        self.stats.gaps_were_fixed = True

        return 0

    def set_channelid(self, channelid, update_filenames=True):
        """Set channel in volumes array and edit filenames."""
        old_channelid = self.stats.channelid
        self.volumes['ChannelID'] = channelid
        if update_filenames:
            for f in self.files:
                f['name'] = f['name'].rstrip(old_channelid) + channelid
        self.stats.channelid = channelid

    def smooth_clock_corrections(self, step=1):
        """Smooth and distribute clock corrections evenly amongst volumes.

        ..Note:: Clock corrections are stored with a maximum resolution
            of 10e-4 seconds (i.e. 100 usec increments).

        Parameters
        ----------
        step : int >= 1 or 'delta', optional
            Round clock corrections to `step` increments, where an increment is
            100 usec. Specify 'delta' to round to the data sample interval.
            Default is 1, the maximum resolution.

        """
        # Check if there is anything to do
        if self.clock_corrs.size < 2:
            return

        if self.stats.clock_corrs_smoothed != 0:
            raise RuntimeError("Clock corrections already smoothed")

        if self.has_gaps:
            raise RuntimeError(
                "Cannot smooth clock corrections while data contain time "
                "gap errors.")

        # check the inputs
        if step == 'delta':
            step = self.stats.delta
        else:
            try:
                step = int(step)
                assert step >= 1
            except (ValueError, AssertionError):
                raise ValueError("step must be 'delta' or an int > 1")

        t = self.times.astype(np.int64)
        cc_times = self.times[self.clock_corrs['i']].astype(np.int64)
        cc_corrs = self.clock_corrs['corr'].astype(np.int64)

        # linearly interpolate between clock correction updates
        cc_smoothed = np.int64(np.round(np.interp(t, cc_times, cc_corrs)))

        if step > 1:
            cc_smoothed = np.int64(np.trunc(cc_smoothed / step)) * step

        self.volumes['T_corr'] = cc_smoothed
        self.stats.clock_corrs_smooth = step

        self.__set_time_arrays__()

    def apply_clock_corrections(self, force=False):
        """Apply the 'T_corr' header to header start times.

        Obspy cannot identify and apply clock corrections (cc's) when they
        occur within a file. So apply the cc's to the start and and blank the
        'T-Corr' word. Sub-sample time shifts are not handled well so
         smooth/interpolate the cc's to sample interval increments.

        """
        if self.stats.clock_corrs_applied and not force:
            raise RuntimeError('clock corrections have already been applied')

        self.times += self.volumes['T_corr'].astype('timedelta64[100us]')
        self.volumes['T_corr'] = 0
        self.__write_times_to_headers__()
        self.stats.clock_corrs_applied = True
        self.__set_time_arrays__()

    def get_bytes(self, istart=None, iend=None, renumber=True):
        """Read sample data, merge in headers and return as bytes object.

        Parameters
        ----------
        istart : int, optional
            Write data from index `start` onwards. Default = `None`, write data
            from the start of `volumes` array.
        iend : int, optional
            Write data up to index but excluding `iend`. Default = `None`,
            write all to the iend of `volumes` array.
        renumber : bool, optional
            Renumber `Sequence Number` field in fixed headers so that
            it increases from 0 as expected. Default = `True`.

        """
        if istart is None:
            i0 = 0
        else:
            i0 = int(istart)
            if istart < 0:
                i0 = 0

        if iend is None:
            i1 = self.volumes.size
        else:
            i1 = int(iend)
            if iend > self.volumes.size:
                i1 = self.volumes.size

        # define the block
        if renumber:
            block_hdrs = self.volumes[i0:i1].copy()
            block_hdrs['SeqNum'] = \
                ["{:06d}".format(n) for n in range(block_hdrs.size)]
        else:
            block_hdrs = self.volumes[i0:i1]

        # get the files we need
        i_files = [f['index'][0] for f in self.files[1:]]
        a = np.digitize((i0, ), i_files)[0]
        b = np.digitize((i1, ), i_files, right=True)[0]

        block_files = self.files[a:b + 1]

        block_size = self.stats.block_nbytes
        hdr_size = self.dtypes['hdrs'].itemsize
        data_size = block_size - hdr_size

        # read files into an io buffer
        with io.BytesIO() as raw:

            # read the bits we need
            for f in block_files:
                f0, f1 = f['index']
                nread = f['n']
                skip = 0
                if f0 < i0:
                    skip = i0 - f0
                    nread -= skip
                if f1 > i1:
                    nread -= (f1 - i1)

                if nread:
                    with open(f['name'], 'rb') as fh:
                        if skip:
                            fh.seek(skip * block_size, 0)
                        raw.write(fh.read(nread * block_size))

            # rewind to start of buffer and
            # then roll through overwriting headers
            raw.seek(0, io.SEEK_SET)
            for hdr in block_hdrs:
                raw.write(hdr.tobytes())  # write the headers
                raw.seek(data_size, 1)  # skip the samples

            return raw.getvalue()

    def write(self, fname, istart=None, iend=None, renumber=True):
        """Write volumes to a single file, optionally choosing a subset of data.

        Parameters
        ----------
        fname : str or io.BytesIO
            Output file name or open
        istart : int, optional
            Write data from index `start` onwards. Default = `None`, write data
            from the start of `volumes` array.
        iend : int, optional
            Write data up to index but excluding `iend`. Default = `None`,
            write all to the iend of `volumes` array.
        renumber : bool, optional
            Renumber `Sequence Number` field in fixed headers so that
            it increases from 0 as expected. Default = `True`.

        """
        if not isinstance(fname, io.BytesIO):
            fh2 = open(fname, 'wb')
        else:
            fh2 = fname

        nn = fh2.write(
            self.get_bytes(istart=istart, iend=iend, renumber=renumber))

        if not isinstance(fname, io.BytesIO):
            fh2.close()

        return nn // self.stats.block_nbytes

    def write_updated_input_files(self,
                                  output_dir=None,
                                  fix_file_names=False,
                                  overwrite=False):
        """Write corrected input files back to disk.

        This method allows for 'cleaned up' input files to be written back
        to disk.

        Parameters
        ----------
        output_dir : str or None, optional
            Path where output files will be written.
            default: None, output path same as input file path.
        fix_file_names : bool, optional
            Save to the 'correct' file name as defined in LPR-200.
            specifications/manual. Default is `False`, do not rename.
        overwrite : bool, optional
            Overwrite existing files, i.e. clobber input files.
            default: False, do not overwrite existing files.

        Returns
        -------
        out : list
            Names of the miniseed files written.

        """
        output_files = []
        for i, f in enumerate(self.files):

            # generate new file name if necessary
            input_dir, input_basename = os.path.split(f['name'])

            if not fix_file_names:
                output_basename = input_basename
            else:
                t = f['starttime'].strftime("%y%m%d%H%M%S")
                output_basename = "{}{}.{}".format(self.stats.stationid, t,
                                                   self.stats.channelid)

            # change output_dir if necessary
            if output_dir:
                output_file = os.path.join(output_dir, output_basename)
            else:
                output_file = os.path.join(input_dir, output_basename)

            # make output dir if necessary
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            elif not os.path.isdir(output_dir):
                raise NotADirectoryError(
                    "creating directory '{}': file already "
                    "exists and is not a dir".format(output_dir))

            if not overwrite and os.path.exists(output_file):
                raise IOError(
                    "writing '{}': file already exists".format(output_file))

            output_files.append(output_file)

            self.write(output_file, istart=f['index'][0], iend=f['index'][1])

        return output_files

    def write_cc_chunks(self, output_dir='.', suffix='', prefix='', index=''):
        """Split `volumes` into chunks with constant clock correction.

        Output files will be saved under `output_dir` and have sequentially
        numbered file names of the form ``prefix.####.suffix``. Optionally
        generate an index file so that the chunk start points can be located
        when/if the chunks are merged (e.g when using `GapFixer` utility
        class).

        Parameters
        ----------
        output_dir : str, optional
            Path where output files will be written. Default = `./`,  the
            current dir.
        suffix : str, optional
            Suffix appended to output file names. Default is
            `StationId.ChannelId`, e.g. `SS01.EHE`.
        prefix : str, optional
            Prefix to be pre-pended to output file names.
            Default is '', no prefix
        index : str, optional
            Write a simple index file of ``time tcorr`` pairs to file `index`.
            Default is `''`, no index file generated.

        Returns
        -------
        out : list
            Names of the miniseed files written.

        """
        if not suffix:
            suffix = "{}.{}".format(self.stats.stationid, self.stats.channelid)

        output_dir = str(output_dir)
        output_fnames = []
        ncc = self.clock_corrs.size
        counter_fmt = '{{:0{}d}}'.format(int(np.log10(ncc)))

        for i in range(ncc):

            # get start and endpoint of block
            start = self.clock_corrs[i]['i']
            if i + 1 < ncc:
                end = self.clock_corrs[i + 1]['i']
            else:
                end = self.volumes.size

            # generate an output file name, starting with a counter
            output_fname = counter_fmt.format(i)
            if prefix:
                output_fname = '{}{}'.format(prefix, output_fname)
            if suffix:
                output_fname = '{}{}'.format(output_fname, suffix)

            output_fname = os.path.join(output_dir, output_fname)
            output_fnames.append(output_fname)

            self.write(output_fname, istart=start, iend=end)

        if index:
            self.write_cc_index(index)

        return output_fnames

    def write_cc_index(self, path='.'):
        """Write an index of clock correction updates to `fname`.

        Index will contain space separated ``time corr`` pairs.
        Units are (utc, microsecond).

        """
        j0 = datetime64_to_utc(self.starttime).strftime('%Y.%j')
        j1 = datetime64_to_utc(self.endtime).strftime('%j')
        fname = "{}.{}.{}-{}.index".format(self.stats.networkid,
                                           self.stats.stationid, j0, j1)
        fname = os.path.join(path, fname)

        with io.StringIO(self.stats.stationid + '\n') as outstr:
            for i, corr in self.clock_corrs:
                tt = np.datetime_as_string(self.times[i])
                outstr.write("{} {}\n".format(tt, np.int64(corr)))

            with open(fname, 'w') as index_file:
                index_file.write(outstr.getvalue())

    @property
    def has_gaps(self):
        """Seismic headers have time jump errors."""
        return (self.gaps is not None and isinstance(self.gaps, np.ndarray)
                and self.gaps.size > 0)

    @property
    def starttime(self):
        """Record start time, i.e. first sample, as numpy.datetime64[100us]."""
        rval = None
        if isinstance(self.times, np.ndarray):
            rval = self.times[0]
        return rval

    @property
    def endtime(self):
        """Record end time, i.e final sample, as as numpy.datetime64[100us]."""
        rval = None
        if isinstance(self.times, np.ndarray):
            rval = self.times[-1] + self.stats.block_len - self.stats.delta
        return rval


# helper functions for dealing with miniseed files


def read_first_header(fname):
    """Read the first header volume into a structured numpy value.

    Field names and types are accessible through the `dtype` attribute.

    Parameters
    ----------
    fname : str
        The file to be read.

    Returns
    -------
    header : `structured numpy.dtype`
        The headers as a structured numpy.dtype with named fields. Field names
        and formats can be accessed via the `header.dtype` attribute.

    """
    raw_hdr = read_headers(fname, count=1)

    # noinspection PyTypeChecker
    hdr1 = np.fromstring(raw_hdr, dtype=LPR200_MSEED_HEADER_DTYPE)[0]
    if hdr1['DataBegin'] != 56:
        raise LPR200FormatError(
            "reading '{}': cannot determine byte order. Not an lpr200 "
            "miniseed file?".format(fname))
    return hdr1


# def get_file_starttime(fname, utc=False):
#     """Read first fixed header in ANU mseed file and return start time.
#
#     Parameters
#     ----------
#     fname : str
#         The miniseed file to read
#     utc: bool, optional
#         Convert datetime64 to UTCDateTime.
#
#     Returns
#     -------
#         start_time : `datetime64` or `obspy.UTCDateTime`
#
#     """
#     t = hdr_to_datetime64(read_first_header(fname))
#     if utc:
#         t = datetime64_to_utc(t)
#
#     return t


def hdr_to_datetime64(hdr):
    """Extract BTIME word from miniseed header and convert to datetime64."""
    if isinstance(hdr, bytes):
        hdr = np.frombuffer(hdr[:56], dtype=LPR200_MSEED_HEADER_DTYPE)

    t = np.datetime64(int(hdr['y']) - 1970, 'Y') + \
        np.timedelta64(int(hdr['j']) - 1, 'D') + \
        np.timedelta64(int(hdr['H']), 'h') + \
        np.timedelta64(int(hdr['M']), 'm') + \
        np.timedelta64(int(hdr['S']), 's') + \
        np.timedelta64(int(hdr['us']), '100us')
    return t


def datetime64_to_utc(t):
    """Convert `numpy.datetime64` instance `t` to `obspy.UTCDateTime`."""
    return obspy.UTCDateTime(np.datetime_as_string(t))


def is_lpr200_mseed(fname):
    """Determine if `fname` is a LPR200 flavoured miniseed file."""
    # any read errors etc will throw exception => False
    try:
        # read first header and test if SeqNum and data_type and reclen
        # words are good. These are string and uchar's so byteorder is
        # irrelevant.
        raw = read_headers(fname, count=1, check=False)
        seqnum, data_type, reclen = struct.unpack(b'6sc47xB', raw[:55])

        # check that file size is appropriate
        assert os.path.getsize(fname) % (2**reclen) == 0
        # data type at byte 7 should be 'D' == char 68
        assert data_type == b'D'
        # bytes 1-6 = SeqNum string
        # should be empty or zero padded integer str of length 6
        seqnum = seqnum.strip()
        assert not seqnum or seqnum.isdigit()
    except (AssertionError, OSError, ValueError, struct.error):
        return False
    return True


def read_headers(fname, block_size=8192, count=0, check=True):
    """Read volume and blockette headers from LPR-200 miniseed file.

    Parameters
    ----------
    fname : str
        The file to read.
    block_size: int, optional
        Byte size of a single data volume (includes `header_size`). Default is
        8192 bytes, i.e 2**13.
    count: int, optional
        Number of headers to read. Default is 0, read all headers.
    check : bool, optional
        Perform tests to ensure that `fname` is a valid LPR-200 miniseed file

    Returns
    -------
    headers : bytes
        The headers as a bytes literal.

    """
    hs = 56
    block_size = int(block_size)
    file_size = os.path.getsize(fname)

    # slurp file - 1 read then buffer manipulation faster than multiple
    # disk reads, especially over nfs
    with open(fname, 'rb') as fh:

        if not block_size:
            hdr1 = fh.read(hs)[54]
            # noinspection PyTypeChecker
            block_size = 2**struct.unpack(b'>B', hdr1)[0]
            fh.seek(0)

        if check and file_size % block_size != 0:
            raise LPR200FormatError(
                "reading '{}': file size {} inconsistent with specified "
                "block size {}.".format(fname, file_size, block_size))

        nblocks = int(file_size // block_size)
        if not count or count > nblocks:
            count = nblocks

        raw_bytes = fh.read(count * block_size)

    # get headers
    raw_hdr = []
    for i in range(0, file_size, block_size):
        raw_hdr.append(raw_bytes[i:i + hs])

    return b''.join(raw_hdr)


class StatsDict(dict):
    def __init__(self, *args, **kwargs):
        super(StatsDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

        # default values
        self.block_nbytes = 0
        self.block_nsamp = 0
        self.block_len = 0
        self.samplerate = 0.
        self.delta = 0.
        self.channelid = ''
        self.stationid = ''
        self.networkid = ''
        self.clock_corrs_smoothed = 0
        self.gaps_were_fixed = False
        self.clock_corrs_applied = False
