"""Utility classes for fixing LPR-200 errors."""

import os
import sys
import io
import numpy as np
import obspy

from lpr200.miniseed import (MiniSeed, is_lpr200_mseed, read_first_header,
                             datetime64_to_utc, hdr_to_datetime64)


class GapFixer:
    """Remove gaps and smooth clock corrections in miniseed files.

    Search for miniseed files below `top` dir.

    Parameters
    ----------
    top : str
        Top level directory. All files below this are checked and 'indexed'
        by station -> channel -> [files...].
    step : int, 'delta' or None, optional
        Resample clock corrections to step intervals.
        see Miniseed.smooth_clock_corrections for details
    max_gap: int
        Maximum permissible time gap (in seconds) between successive headers
        that will be repaired. Default is 600 seconds, i.e 10 minutes)
    check_cc : bool
        Cross check found 'breaks' against clock corrections. (Default is True,
        do cross checking).
    file_namer : function object or None
        A function to generate output file names from trace metadata.
        The function will receive an obspy.Trace as an argument.
        Default: None, use built-in naming function.

    Attributes
    ----------
    index : dict
        Dictionary of all miniseed files below `top`. Key hierarchy is
        ``{StationID1: {chan1: [files...], chan2: [files, ...], ...},
        StationID2: ...}``
    top : str
        The top level directory.
    file_namer : function
        Function used to name output files
    verbose : bool
        Print progress messages.

    """

    def __init__(self,
                 top,
                 step='delta',
                 max_gap=600,
                 check_cc=True,
                 file_namer=None,
                 verbose=True):
        self.index = {}
        self.top = top
        self.__file_namer__ = file_namer
        self.step = step
        self.max_gap = int(max_gap)
        self.check_cc = bool(check_cc)
        self.verbose = bool(verbose)

        # form the index
        self._msg('Creating index', True)
        for root, dirs, files in os.walk(self.top):
            for f in files:
                f = os.path.join(root, f)
                if not is_lpr200_mseed(f):
                    continue
                vol1 = read_first_header(f)
                station = vol1['StationID'].decode('ascii').strip().upper()
                channel = vol1['ChannelID'].decode('ascii').strip().upper()
                if station not in self.index:
                    self.index[station] = {}
                if channel not in self.index[station]:
                    self.index[station][channel] = []
                self.index[station][channel].append(f)

        self._msg("Processing files for the following Stations and channels:",
                  True)
        if self.verbose:
            for s in self.index:
                m = '{}: {}'.format(s, ", ".join(list(self.index[s].keys())))
                self._msg(m)

    @property
    def file_namer(self):
        if self.__file_namer__ is None:
            return self.default_namer
        else:
            return self.__file_namer__

    @staticmethod
    def default_namer(tr):
        """File naming function used if none specified.

        Returns 'network.station.channel.year.julian_day'

        """
        return "{}.{}.{}.{}".format(tr.stats.network, tr.stats.station,
                                    tr.stats.channel,
                                    tr.stats.starttime.strftime('%Y.%j'))

    def _msg(self, msg, blank_line=False):
        """Print verbose messages."""
        if self.verbose:
            if blank_line:
                print('', file=sys.stderr)
            print(msg, file=sys.stderr)

    def fix_all(self, output_dir, length='D'):
        """Fix gap & clock errors for all files."""
        for station in self.index:
            self.fix_station(
                station, channels=(), output_dir=output_dir, length=length)

    def fix_station(self, station, channels=(), output_dir='.', length='D'):
        """Fix gap & clock errors for a given station and channel.

        Parameters
        ----------
        station : str
            The station to process.
        channels : array_like
            The channel(s) to process.
        output_dir : str
            Directory where files, including temporary files will be written.
        length : {'D', 'h', int}, optional
            Desired record length in seconds. Alternately specify 'D' or
            'hour'. Default is 'D', i.e. 86400 seconds.

        """
        if station not in self.index:
            raise ValueError("Found no files for station '{}'".format(station))
        if not channels:
            channels = sorted(list(self.index[station].keys()))
        else:
            if isinstance(channels, str):
                channels = (channels, )

            for ch in channels:
                if ch not in self.index[station]:
                    raise ValueError(
                        "Found no files for station '{}', channel '{}'".format(
                            station, ch))

        if length == 'D':
            # rec_len = 86400
            dt64_fmt = 'D'
        elif length == 'h':
            # rec_len = 3600
            dt64_fmt = 'h'
        else:
            try:
                rec_len = int(length)
                assert rec_len > 0
            except (ValueError, AssertionError):
                raise ValueError("arg length must be an int>0, 'D' or 'h")
            dt64_fmt = '{}s'.format(rec_len)

        self._msg("Creating output_dir '{}'".format(output_dir), True)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        elif not os.path.isdir(output_dir):
            raise NotADirectoryError(
                "creating directory '{}': file already "
                "exists and is not a dir".format(output_dir))

        # read first channel and fix it
        for n_pass, ch in enumerate(channels):

            self._msg('Processing: {} {}'.format(station, ch), False)

            # Pass 1: read headers and correct times, only do this once
            if n_pass == 0:
                mseed1 = MiniSeed(*self.index[station][ch])
                mseed1.fix_gap_errors(
                    max_gap=self.max_gap, check_cc=self.check_cc)
                mseed1.smooth_clock_corrections(step=self.step)

                # write index before we strip/apply clock corrections
                mseed1.write_cc_index(output_dir)

                mseed1.apply_clock_corrections()

                # noinspection PyTypeChecker
                t_starts = np.arange(
                    np.datetime64(mseed1.starttime, dt64_fmt),
                    np.datetime64(mseed1.endtime, dt64_fmt) + 1)
                t_delta = np.timedelta64(mseed1.stats.delta, '100us')

            # Pass 2...: headers are identical except for channel,
            # so reset channelid and file names
            else:
                # noinspection PyUnresolvedReferences,PyUnboundLocalVariable
                mseed1.set_channelid(ch, update_filenames=True)

            self._msg('Writing output files', True)

            # noinspection PyUnboundLocalVariable
            for n, t0 in enumerate(t_starts):
                t1 = t0 + 1

                if t0 < mseed1.starttime:
                    t0 = mseed1.starttime
                    istart = 0
                else:
                    istart = np.where(mseed1.times <= t0)[0][-1]
                t0_utc = datetime64_to_utc(t0)

                if t1 >= mseed1.endtime:
                    t1 = mseed1.endtime
                    iend = mseed1.volumes.size
                    t1_utc = datetime64_to_utc(t1)
                else:
                    iend = np.where(mseed1.times >= t1)[0][0]
                    # noinspection PyUnboundLocalVariable
                    t1_utc = datetime64_to_utc(t1 - t_delta)

                with io.BytesIO(mseed1.get_bytes(istart, iend)) as data:
                    data2 = obspy.read(data, format='MSEED')

                data2.sort()
                data2.merge(
                    method=1,
                    interpolation_samples=-1,
                    fill_value='interpolate')
                if len(data2) != 1:
                    raise RuntimeError(
                        'Could not cleanly merge clock corrected blocks.')

                tr = data2.slice(starttime=t0_utc, endtime=t1_utc)[0]
                output_filename = os.path.join(output_dir, self.file_namer(tr))

                tr.write(output_filename, format="MSEED")


class BadFileFixer(object):
    """Utility class to rename and 'fill' missing miniseed files.

    Mis-named and missing files are typically caused by write errors, possibly
    due to a bad SD card.

    Parameters
    ----------
    top : str
        Top level directory to search.
    channel_codes : array-like, optional
        Array of valid channel codes/names. Default codes are
        ``('EHN', 'EHE', 'EHZ')``
    dry_run : bool, optional
        Don't write anything. Default = True.
    print_summary : bool, optional
        Print a summary of the actions performed. Default = True.

    Attributes
    ----------
    index : dict
        Dictionary of all miniseed files below `top`. Key hierarchy is
        ``{StnID1: {time1: {chan1: fname, chan2: fname,...}, time2...},
        StnID2: ...}``

    """

    def __init__(self,
                 top,
                 channel_codes=('EHN', 'EHE', 'EHZ'),
                 dry_run=True,
                 print_summary=False):

        self.top = top
        self.channel_codes = channel_codes
        self.print_summary = bool(print_summary)
        self.dry_run = bool(dry_run)
        self.index = {}
        self.actions = []

        if not os.path.isdir(top):
            raise NotADirectoryError("No such directory: '{}'".format(top))

        for root, dirs, files in os.walk(self.top):
            for f in files:
                fname = os.path.join(root, f)
                if not is_lpr200_mseed(fname):
                    continue

                vol1 = read_first_header(fname)

                starttime = datetime64_to_utc(hdr_to_datetime64(vol1))
                starttime = str(starttime).upper()

                station = vol1['StationID'].decode('ascii').strip().upper()
                channel = vol1['ChannelID'].decode('ascii').strip().upper()

                if station not in self.index:
                    self.index[station] = {}

                # noinspection PyTypeChecker
                if starttime not in self.index[station]:
                    self.index[station][starttime] = {}
                if channel in self.index[station][starttime]:
                    raise ValueError("Correcting '{}': channel already exists"
                                     .format(fname))
                self.index[station][starttime].update({channel: fname})
        self.__check_for_multiple_files__()

    def __check_for_multiple_files__(self):
        """Ensure that at most 3 files for each timestamp in index.

        If > 3 files, then we have a problem requiring manual user
        intervention. So, raise RuntimeError.

        """
        n = len(self.channel_codes)
        for stn in self.index:
            for starttime in self.index[stn]:
                if len(self.index[stn][starttime]) > n:
                    raise RuntimeError(
                        "Whoaa! Too many files. Not touching this!!")

    def _find_weird_file_names(self):
        """Find weird file names."""
        weird_names = []
        for stn in self.index:
            for files in self.index[stn].values():
                for fname in files.values():
                    try:
                        os.path.basename(fname).encode('ascii')
                    except UnicodeEncodeError:
                        weird_names.append(fname)
        return weird_names

    def fix_weird_file_names(self):
        """Fix non-ascii characters in file names."""
        actions = []
        for stn in self.index:
            for starttime, files in self.index[stn].items():
                good_fnames = []
                bad_fnames = []
                for chan, fname in files.items():
                    try:
                        os.path.basename(fname).encode('ascii')
                    except UnicodeEncodeError:
                        bad_fnames.append(chan)
                    else:
                        good_fnames.append(chan)
                # rename bad files
                template = ''
                for b in bad_fnames:
                    old_fname = files[b]
                    if not template:
                        if not good_fnames:
                            t = obspy.UTCDateTime(starttime)
                            template = stn + t.strftime('%y%m%d%H%M%S')
                        else:
                            template = os.path.basename(files[good_fnames[0]])
                            template = template.rstrip(good_fnames[0])

                    new_fname = os.path.join(
                        os.path.dirname(old_fname), template + b)
                    if not self.dry_run:
                        os.rename(old_fname, new_fname)
                        self.index[stn][starttime][b] = new_fname
                    actions.append(('rename', old_fname, new_fname))

        if not self.dry_run:
            self.actions.extend(actions)
        if self.print_summary:
            for action, old, new in actions:
                print("{:<7}: {}".format(action, old))
                print("{:^7}  {}".format('==>', new))

    def fix_missing_files(self):
        """Create dummy files to 'fill in' the missing files.

        Requires that for a given missing file, say 'EHE' component data,
        there exist files for the other components. Such files will have
        identical gap errors and time corrections. So,

        * copy and rename one of these files
        * zero the data and set the `ChannelID` header to appropriate value.

        """
        actions = []
        n = len(self.channel_codes)
        for stn in self.index:
            for starttime, files in self.index[stn].items():
                if len(files) >= n:
                    continue
                missing = [c for c in self.channel_codes if c not in files]
                present = [c for c in self.channel_codes if c in files]
                if not present:
                    print("Error fixing:", file=sys.stderr)
                    print(
                        'files: ', [f for f in files.values()],
                        file=sys.stderr)
                    print(
                        'channel_codes: ', self.channel_codes, file=sys.stderr)
                    raise ValueError(
                        "Cannot fill missing file. No existing files to copy "
                        "from. You will have to intervene.")
                src_fname = files[present[0]]
                prefix = src_fname.rstrip(present[0])

                for channel in missing:
                    new_fname = prefix + channel
                    # read file, zero data and rename channel
                    if not self.dry_run:
                        src = MiniSeed(src_fname)
                        null_bytes = b'\x00' * (src.stats.block_nbytes - 56)
                        src.set_channelid(channel, update_filenames=True)
                        hdr_bytes = [v.tobytes() for v in src.volumes]

                        with open(new_fname, 'wb') as fh:
                            fh.write(null_bytes.join(hdr_bytes) + null_bytes)

                        self.index[stn][starttime][channel] = new_fname
                    actions.append(('copy', src_fname, new_fname))

        if not self.dry_run:
            self.actions.extend(actions)

        if self.print_summary:
            for action, old, new in actions:
                print("{:<7}: {}".format(action, old))
                print("{:^7}  {}".format('==>', new))
