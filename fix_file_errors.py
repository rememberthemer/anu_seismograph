"""Script for fixing missing file and naming errors."""

import os
import lpr200

if __name__ == '__main__':

    station_id = 'SS18'
    base = 'data'
    top = os.path.join(base, station_id)

    fixer = lpr200.BadFileFixer(top)
    fixer.print_summary = True
    fixer.dry_run = False
    fixer.fix_weird_file_names()
    fixer.fix_missing_files()
