from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
# noinspection PyUnresolvedReferences
from future.builtins import *

import setuptools

setuptools.setup(
    name="lpr200",
    packages=setuptools.find_packages()
)
# install_requires=['numpy', 'obspy']
