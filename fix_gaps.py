"""Script for fixing time errors in LPR-200 miniseed files.

Fixes gaps within miniseed volumes and regularises the application of
clock corrections.

"""
import os
import lpr200


def fname_func(tr):
    """Generate filename from `obspy.Trace` object.

    An example `file_namer` function as used by `lpr200.GapFixer` class.

    """
    data = [tr.stats.network, tr.stats.station, 'D', tr.stats.channel]
    if tr.count() == 8640000:
        data.append(tr.stats.starttime.strftime('%Y.%j'))
    else:
        data.append(tr.stats.starttime.strftime('%Y.%j.%H%M%S'))
    return str.join('.', data)


if __name__ == '__main__':

    station_id = "SS28"
    basepath = '/home/bensonad/workspace/anu_seismograph/data'
    output_dir = os.path.join(basepath, 'junk',
                              station_id, station_id + ' miniSEED_fixed')
    rec_len = 'D'

    top = os.path.join(basepath, station_id, station_id + ' miniSEED')
    print(top)
    fixer = lpr200.utils.GapFixer(top, file_namer=fname_func)
    fixer.verbose = True
    fixer.fix_station(station_id, output_dir=output_dir, length=rec_len)
    # fixer.fix_channel(station_id, 'EHE', output_dir, record_length=rec_len)
